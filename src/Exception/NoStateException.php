<?php

namespace Raajen\OAuth2\Dropbox\Client\Exception;

use Exception;

class NoStateException extends Exception
{
}
