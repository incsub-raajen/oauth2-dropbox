<?php

namespace Raajen\OAuth2\Dropbox\Client\Exception;

use Exception;

class InvalidArgumentException extends Exception
{
}
